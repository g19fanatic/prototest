#include <google/protobuf/descriptor.h>
#include <google/protobuf/descriptor.pb.h>
#include <google/protobuf/dynamic_message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>

#include <main.pb.h>
#include <sentMessage.pb.h>
#include <Master.pb.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

using namespace std;
using namespace google::protobuf;

void printMsg(const Message& msg, string preText = "")
{
    const Reflection* reflection = msg.GetReflection();
    const Descriptor* descriptor = msg.GetDescriptor();

    cout << preText << "Message: " << descriptor->full_name() << endl;
    stringstream ss;
    ss << preText << "  ";
    preText = ss.str();

    vector<const FieldDescriptor*> fields;

    reflection->ListFields(msg,&fields);

    for (unsigned int i = 0; i < fields.size(); ++i)
    {
        string strName;
        string strValue;
        bool toPrint = true;

        const Message* tempMsg;
        const FieldDescriptor* curFd = fields.at(i);

        switch (curFd->type())
        {
        case FieldDescriptor::TYPE_MESSAGE:
            toPrint = false;
            tempMsg = &reflection->GetMessage(msg,curFd);
            printMsg(*tempMsg, preText);
            break;
        case FieldDescriptor::TYPE_BYTES:
        case FieldDescriptor::TYPE_STRING:
            strName = curFd->full_name();
            strValue = reflection->GetString(msg,curFd);
            break;
        default:
            break;
        }

        if (toPrint)
        {
            cout << preText << strName << " = " << strValue << endl;
        }
    }
}

void genSavedData()
{
    MainMsg mMsg;

    InterestingMessage* iMsg = mMsg.mutable_imsg();
    iMsg->set_msg("Interesting! Hello!");

    BoringMessage* bMsg = mMsg.mutable_bmsg();
    bMsg->set_msg("Boring... Good Bye...");

    ofstream mainOut("../log/main.pbe",ios_base::binary|ios_base::trunc);
    mMsg.SerializeToOstream(&mainOut);
    mainOut.close();

    DescribedMessage dMsg;
    dMsg.set_full_name(mMsg.descriptor()->full_name());
    string rawMainMsg;
    mMsg.SerializeToString(&rawMainMsg);
    dMsg.set_message(rawMainMsg);

    ofstream dOut("../log/describedMsg.pbe",ios_base::binary|ios_base::trunc);
    dMsg.SerializeToOstream(&dOut);
    dOut.close();

    cout << "\nPrinting the saved main msg\n";
    printMsg(mMsg);
}

int main()
{
    genSavedData();

    FileDescriptorSet fds;
    ifstream desc("../res/msgs.desc",ios_base::binary);
    if (!fds.ParseFromIstream(&desc))
    {
        cout << "Error Parsing the description set file\n";
    }
    desc.close();

    DescriptorPool dp;
    vector<const FileDescriptor*> vecFDs;
    for (int i = 0; i < fds.file_size(); ++i)
    {
      vecFDs.push_back(dp.BuildFile(fds.file(i)));
    }

    DynamicMessageFactory dmf(&dp);

    DescribedMessage descMsg;

    ifstream descMsgIn("../log/describedMsg.pbe",ios_base::binary);
    descMsg.ParseFromIstream(&descMsgIn);
    descMsgIn.close();

    const Message* msgProto = dmf.GetPrototype(dp.FindMessageTypeByName(descMsg.full_name()));
    Message* msg = msgProto->New();
    msg->ParseFromString(descMsg.message());

    cout << "\nPrinting the dynamically parsed main message\n";
    printMsg(*msg);

    google::protobuf::internal::ExtensionSet eSet;

    eSet.RegisterMessageExtension(&::MasterMessage::default_instance(),1,google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,false,false,msgProto->New());
    google::protobuf::internal::ExtensionIdentifier<MasterMessage,
                                                    google::protobuf::internal::MessageTypeTraits< Message >,
                                                    google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
                                                    false > msgExtIdent(1, msgProto->New());

    MasterMessage masterMsg;
    const Reflection* masterReflection = masterMsg.GetReflection();
    vector<const FieldDescriptor *> masterFieldsList;
    masterReflection->ListFields(masterMsg,&masterFieldsList);

    Message* mainMessage = masterMsg.MutableExtension(msgExtIdent);

    /*
    for (unsigned int i = 0; i < masterFieldsList.size(); ++i)
    {
        const FieldDescriptor* curFD = masterFieldsList.at(i);
        if (curFD->full_name() == msgProto->GetTypeName())
        {
            mainMessage = masterReflection->MutableMessage(&masterMsg,curFD);

            break;
        }
    }
    */

    if (mainMessage) { mainMessage->ParseFromString(descMsg.message()); }

    cout << "\nPrinting the dynamically 'created' master message\n";
    printMsg(masterMsg);

    return 0;
}

