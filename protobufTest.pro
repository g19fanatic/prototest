TEMPLATE = app
CONFIG += console
CONFIG -= qt
CONFIG += release

INCLUDEPATH +=  lib/genProtos \
                include

LIBS += -lprotobuf

OBJECTS_DIR = obj/

DESTDIR = bin/

TARGET = protobufTest

SOURCES += \
    src/main.cpp \
    lib/genProtos/main.pb.cc \
    lib/genProtos/interesting.pb.cc \
    lib/genProtos/boring.pb.cc \
    lib/genProtos/sentMessage.pb.cc \
    lib/genProtos/Master.pb.cc \
    lib/genProtos/exampleExtension.pb.cc
